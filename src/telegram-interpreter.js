const httpRequest = require('request-promise')

class TelegramInterpreter {
  constructor(token, dependencies = { httpRequest }) {
    this.token = token
    this.httpRequest = dependencies.httpRequest

    this.request = this.request.bind(this)
  }

  request(action, body) {
    const requestOptions = {
      body,
      json: true,
      method: 'POST',
      uri: `${TelegramInterpreter.BASE_URI}${this.token}/${action}`
    }

    return this.httpRequest(requestOptions)
      .then(apiResponse => console.log(apiResponse))
  }

  translateRequest(payload) {
    if (!payload.text) {
      return 'No message given'
    }

    const startWithSlash = /^\//.test(payload.text)
    if (!startWithSlash) {
      return 'I don\'t understand'
    }

    const requestedAction = TelegramInterpreter.TEXT_REG_EXP.exec(payload.text)

    return {
      name: requestedAction[1],
      parameters: requestedAction[2].trim()
    }
  }

  sendMessage(payloadMessage, botResponse) {
    const rolls = botResponse.values.map(roll => {
      if (roll === 20) {
        return `*${roll}*`
      }

      if (roll === 1) {
        return `_${roll}_`
      }

      return roll
    }).join(', ')

    const formatedResponse = `
_roll_: _${botResponse.shortAttributes}_
_results_: (${rolls})
_total_: ${botResponse.friendlyTotal}`

    const messageBody = {
        chat_id: payloadMessage.chat.id,
        text: `${formatedResponse}`,
        parse_mode: 'Markdown'
    }

    return this.request('sendMessage', messageBody)
  }
}

TelegramInterpreter.TEXT_REG_EXP = /^\/(\w+)([\w\W]*)/
TelegramInterpreter.BASE_URI = 'https://api.telegram.org/bot'

module.exports = TelegramInterpreter
